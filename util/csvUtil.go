package util

import (
	"bufio"
	"encoding/csv"
	"io"
	"log"
	"os"
)

type Schedule struct {
	Month string
	Day   string
	Duty  string
	Year  string
}

func GetEventsAsArray() []Schedule {
	csvFile, _ := os.Open("data/data.csv")
	reader := csv.NewReader(bufio.NewReader(csvFile))
	var schedules []Schedule
	for {
		line, error := reader.Read()
		log.Println(line)
		if error == io.EOF {
			break
		} else if error != nil {
			log.Fatal(error)
		}
		schedules = append(schedules, Schedule{
			Month: line[0],
			Day:   line[1],
			Year:  line[2],
			Duty:  line[3],
		})
	}
	return schedules
}
