# Google API colors

the follwoing assos^ciation shows the values that match a color in google calendar api.

```
11 --> red
10 --> green
9 --> blue
8 --> gray
7 --> light blue
6 --> orange
5 --> yellow
4 --> flamingo
3 --> grape
2 --> sage
1 --> lavender
0 --> calendar color
```

the values can used when creating the event. e.g.:

```go
event := &calendar.Event{
		Summary:     summary,
		Location:    location,
		Description: description,

		Start: &calendar.EventDateTime{
			Date:     date,
			TimeZone: "Europe/Zurich",
		},
		End: &calendar.EventDateTime{
			Date:     date,
			TimeZone: "Europe/Zurich",
		},
        ColorId: colorID, // here you can set the color value as string
```