package calendargo

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"

	"gitlab.com/tribock/calendar_importer/util"
	"golang.org/x/net/context"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/calendar/v3"
)

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config) *http.Client {
	tokFile := "token.json"
	tok, err := util.TokenFromFile(tokFile)
	if err != nil {
		tok = util.GetTokenFromWeb(config)
		util.SaveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

func CreateEvent(month, day, year, summary string) error {
	date := fmt.Sprintf("%s-%s-%s", year, month, day)
	log.Println(date)
	color := getColorIDByDuty(summary)
	event := &calendar.Event{
		Summary:     summary,
		Location:    "Arosa Bergbahnen",
		Description: "Arbeitsplan Seraina",

		Start: &calendar.EventDateTime{
			Date:     date,
			TimeZone: "Europe/Zurich",
		},
		End: &calendar.EventDateTime{
			Date:     date,
			TimeZone: "Europe/Zurich",
		},
		ColorId: color, // see google_calendar_colors.md

		// Recurrence: []string{"RRULE:FREQ=DAILY;COUNT=1"},
		// Attendees: []*calendar.EventAttendee{
		// 	&calendar.EventAttendee{Email: "some.address@gmail.com"},
		// },
	}

	calendarId := "ba0sbmgq1k29t6mdh3efpldoa4@group.calendar.google.com"
	srv := createServer()
	event, err := srv.Events.Insert(calendarId, event).Do()
	if err != nil {
		log.Println("Unable to create event. %v\n", err)
		return err

	}
	fmt.Printf("Event created: %s\n", event.HtmlLink)
	return err

}

func createServer() *calendar.Service {
	b, err := ioutil.ReadFile("credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}

	// If modifying these scopes, delete your previously saved token.json.
	config, err := google.ConfigFromJSON(b, calendar.CalendarEventsScope)
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	client := getClient(config)
	srv, err := calendar.New(client)
	if err != nil {
		log.Fatalf("Unable to retrieve Calendar client: %v", err)
	}
	return srv

}

func getColorIDByDuty(summary string) string {
	switch strings.ToUpper(summary) {
	case "R":
		return "5"
	case "BO":
		return "10"
	case "X":
		return "4"
	default:
		return "0"
	}
}
